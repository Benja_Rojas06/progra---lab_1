#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

# Primeras dos partes del ejercicio:

"""
def calculo(promedio):
	
	print("\nLos números que ingresó son: ", promedio)
	
	suma = sum(promedio)
	total_numeros = len(promedio)
	promedio_total = (suma/total_numeros)

	print("\nEl promedio de los números es: ", promedio_total)
	 
def cuadrados(promedio):
	
	cuadrado = promedio
	for i in cuadrado:
		
		print("El cuadrado de", i, "es: ", i ** 2)
	
 
def inicio():
	
	opcion = 'S' 
	
	while True:
		
		try:
			
			promedio = []
			
			while (opcion == 'S' or opcion == 's'):
				
				numero = float(input("Ingrese números y verá la magia: "))
				
				promedio.append(numero)
				
				opcion = str(input("\n¿Desea ingresar otro número? / (S / N): "))
			
			calculo(promedio)
			cuadrados(promedio)
			break
			
		except:
			print("Has ingresado un caracter")
			
inicio()
"""

def contador_letras(cuenta_letras):
	
	for i in cuenta_letras:
		
		numero = len(i)
		print("Las palabras/oraciones que ha ingresado son: ", i, "y su largo es: ", numero)

def inicio():
	
	opcion = 'S' 
	
	while True:
		
		try:
			cuenta_letras = []
			
			while (opcion == 'S' or opcion == 's'):
				
				palabra = str(input("Ingrese una palabra/oración y verá la magia: "))	
				
				cuenta_letras.append(palabra)
				
				opcion = str(input("\n¿Desea ingresar otra palabra/oración? / (S / N): "))
			
			contador_letras(cuenta_letras)
			break
			
		except:
			print("Has ingresado un número")
			
inicio()


